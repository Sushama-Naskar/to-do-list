function completedNode(id){
    let li = document.getElementById(id);
     let input = li.getElementsByTagName('input')[0];
  console.log('completed');
   let element=JSON.parse(localStorage.getItem(id));
   element['checked']="not-checked";
   localStorage.setItem(id, JSON.stringify(element));
    // input.style.color="black";
    input.style.textDecoration="none";



}
function undocompleted(id){
    let li = document.getElementById(id);
     let input = li.getElementsByTagName('input')[0];
    console.log('undo');
    let element=JSON.parse(localStorage.getItem(id));
    element['checked']="checked";
    localStorage.setItem(id, JSON.stringify(element));
    
    // input.style.color="red";
    input.style.textDecoration="line-through";

}
function update(id) {

    let li = document.getElementById(id);
    let editButton = li.getElementsByClassName('edit-button')[0];
    let input = li.getElementsByTagName('input')[0];
    let element=JSON.parse(localStorage.getItem(id));
    element['text']=input.value;
    console.log(element);
    localStorage.setItem(id, JSON.stringify(element));
    input.setAttribute("readonly", "readonly");
    editButton.innerText = "Edit";
}

function editNode(id) {
    let li = document.getElementById(id);
    let editButton = li.getElementsByClassName('edit-button')[0];
    let input = li.getElementsByTagName('input')[0];
    editButton.innerText = "Update";
    input.removeAttribute("readonly");
    input.focus();

}



function removeNode(id) {

    localStorage.removeItem(id);

}


function addToArray(inputText, id) {

    localStorage.setItem(id, JSON.stringify({"text":inputText, "checked":"not-checked"}));
}

function createLi(inputText, id,status) {
    let container = document.getElementById('container');
    const li = document.createElement('li');
    li.id = id;

    const nodeContainer = document.createElement('div');
    nodeContainer.className = 'node-container';

    const input = document.createElement('input');
    input.type = "text";
    input.setAttribute("readonly", "readonly");
    input.value = inputText;
    if(status=="checked"){
        // input.style.color="red";
        input.style.textDecoration="line-through";
    }

    const deleteButton = document.createElement('button');
    deleteButton.className = 'delete-button';
    deleteButton.innerText = "Delete";
    deleteButton.addEventListener('click', () => {
        container.removeChild(li);

        removeNode(id)
    })

    const editButton = document.createElement('button');
    editButton.className = 'edit-button';

    editButton.innerText = "Edit";
    editButton.addEventListener('click', () => {
        if (editButton.innerText === "Edit") {
            editNode(id);
        } else {
            update(id);
        }
    })

    const completedButton = document.createElement('button');
    completedButton.className = 'completed-button';
    if(status=="not-checked"){
        completedButton.innerText = "completed";
    }else{
        completedButton.innerText = "Not completed";
    }
    completedButton.addEventListener('click', () => {
        if(completedButton.innerText==="completed"){
            completedButton.innerText="Not completed";
            undocompleted(id);
            
        }else{
            completedButton.innerText="completed";
            completedNode(id);
        }

        
        
    })

    nodeContainer.appendChild(input);
    nodeContainer.appendChild(editButton);
    nodeContainer.appendChild(completedButton);
    nodeContainer.appendChild(deleteButton);
    li.appendChild(nodeContainer);

    return li;

}

function createNewList(inputText) {
    const container = document.getElementById('container');
    var date = new Date();
    let id = date.getTime();
    const li = createLi(inputText, id,"not-checked");
    container.appendChild(li);

    addToArray(inputText, id);
}

function addTodo() {

    const inputText = document.getElementById("todo-input");
    const btn = document.getElementById('todo-button');
    if (inputText.value) {
        createNewList(inputText.value);
        inputText.value = "";

    } else {
        alert('Add todo');
    }
}

window.onload = loadArray()

function loadArray() {

    const container = document.getElementById('container');
    container.innerHTML = "";
    Object.keys(localStorage).forEach(key => {
      let element=JSON.parse(localStorage.getItem(key));

        const li = createLi(element['text'], key,element['checked']);
        container.appendChild(li);
    });

}
